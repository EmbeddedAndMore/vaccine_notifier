# Vaccine Notifier

## Installation
It can be installed on Linux, Mac and Windows.
to do so, clone the repo:
```sh
git clone https://gitlab.com/EmbeddedAndMore/vaccine_notifier.git
```


just create a virtual environment using virtualenv or conda like this:
```sh
cd vaccine_notifier
virtualenv venv
```
or
```sh
cd vaccine_notifier
conda create -n venv_name python=3.8
```
activate the env in **linux** or **mac** by:
```sh
source venv/bin/activate
```
or in windows by running: 
```sh
\env\Scripts\activate.bat
```
if using conda then:
```sh
conda activate venv_name
```
and then
```sh
pip install -r requirements.txt
```

then got to https://my.telegram.org/auth?to=apps
enter your telegram number and create an app with whatever name you want.
copy the `api_id` and `api_hash` to the exact field in the `main.py` and then:
```sh
python main.py
```

it will show you a notification for specific vaccine that you looking for.
you can modify the list of vaccines inside `main.py` from the filed `acceptable_vaccines`.
by default it only contains `biontech`. add `astrazeneca` and/or `johnson & johnson` as you wish.
you can also ignore some Doctors by their name through `dr_ignore` list
