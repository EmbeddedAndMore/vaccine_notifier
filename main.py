from telethon.tl.functions.contacts import ResolveUsernameRequest
from telethon import TelegramClient, events
import sys
import time

if sys.platform == 'linux':
    import notify2
    notify2.init('Event')
elif sys.platform == 'win32':
    from win10toast import ToastNotifier
    toaster = ToastNotifier()
elif sys.platform == 'darwin':
    import subprocess
    MAC_CMD = '''
    on run argv
    display notification (item 2 of argv) with title (item 1 of argv)
    end run
    '''



dr_ignore = ['hedayatian']
acceptable_vaccines = ['biontech']


def notify(title, message):
    if sys.platform == 'linux':
        n = notify2.Notification(title, message)
        n.show()
    elif sys.platform == 'win32':
        toaster.show_toast(title, message, icon_path=None, duration=10, threaded=True)
        while toaster.notification_active(): time.sleep(0.1)
    elif sys.platform == 'darwin':
        subprocess.call(['osascript', '-e', MAC_CMD, title, message])


def check_and_publish(msg:str):
    for dr in dr_ignore:
        if dr in msg:
            return
    for vaccine in acceptable_vaccines:
        if  vaccine in msg:
            if 'hier buchen:' in msg:
                link =  msg.split('hier buchen:',1)[1] 
                notify(f'{vaccine} Imfstoff found',link)
            else:
                notify(f'{vaccine} Imfstoff found',"no link available")


api_id = 6039042
api_hash = '88b7ecce52266f49f2c59c3b35426819'

client = TelegramClient('session_name', api_id=api_id, api_hash=api_hash)

@client.on(events.NewMessage(chats='https://t.me/corona_impftermine_ess'))
async def my_event_handler(event):
    print(event.raw_text)
    msg = event.raw_text.lower()
    check_and_publish(msg)

@client.on(events.NewMessage(chats='https://t.me/corona_impftermine_dus'))
async def my_event_handler(event):
    print(event.raw_text)
    msg = event.raw_text.lower()
    check_and_publish(msg)

@client.on(events.NewMessage(chats='https://t.me/marmoun_test'))
async def my_event_handler(event):
    print(event.raw_text)
    msg = event.raw_text.lower()
    check_and_publish(msg)

client.start()
client.run_until_disconnected()